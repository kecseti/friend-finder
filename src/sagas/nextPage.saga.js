import { call, put } from 'redux-saga/effects';
import { actions, url } from '../constants'

export default function* (action){
  const index = Number(localStorage.getItem('displayStartIndex'))+10;
  const jwt = localStorage.getItem('token');
  console.log(`${url}/${action.payload}/${Number(index-10)}/${Number(index)}`)
  const payload = yield call(() => fetch(`${url}/${action.payload}/${Number(index-10)}/${Number(index)}`, {headers: {Authorization:jwt}}).then(res => res.json()))
  if(payload.users.length > 0) {
    localStorage.setItem('displayStartIndex', Number(index));
    yield put({
      type: actions.loadNextPageSucces,
      payload,
    });
  }
}