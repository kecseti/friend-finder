import { call, put } from 'redux-saga/effects';
import { actions, url } from '../constants'

export default function* (){
  const jwt = localStorage.getItem('token');
  const payload = yield call(() => fetch(`${url}/users/my`, {headers: {Authorization:jwt}}).then(res => res.json()))
  yield put({
    type: actions.themeChanged,
    payload: payload.user.theme,
  });
}