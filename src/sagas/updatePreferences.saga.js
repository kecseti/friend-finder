import { call, put } from 'redux-saga/effects';
import { actions, url } from '../constants'

export default function* (action){
  const jwt = localStorage.getItem('token');
  const payload = yield call(() => fetch(
    `${url}/preferences`,{
      method: 'POST',
      headers: {
        Authorization:jwt,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        theme : action.payload,
      })
    }
  ).then(res => res.json()))
  yield put({
    type: actions.themeChanged,
    payload: payload.user.theme,
  });
}
