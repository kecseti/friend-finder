import { call, put } from 'redux-saga/effects';
import { actions, url } from '../constants'

export default function* (action){
  const index = localStorage.getItem('displayStartIndex');
  const jwt = localStorage.getItem('token');
  if(index-20 >= 0){
    localStorage.setItem('displayStartIndex', Number(index)-10);
    const payload = yield call(() => fetch(`${url}/${action.payload}/${Number(index)-20}/${Number(index)-10}`, {headers: {Authorization:jwt}}).then(res => res.json()))
    yield put({
      type: actions.loadPreviousPageSucces,
      payload,
    });
  }
}