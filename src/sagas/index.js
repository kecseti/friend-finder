import { takeEvery, all } from 'redux-saga/effects';
import nextPage from './nextPage.saga'
import previousPage from './previousPage.saga'
import friendMaker from './makeFriendship.saga'
import updatePreferences from './updatePreferences.saga'
import weather from './weather.saga'
import loadPreferences from './loadPreferences.saga'
import deleteFriend from './breakFriendship.saga'
import {actions} from '../constants'

export default function* rootSaga() {
  yield all([
    takeEvery(actions.deleteFriend, deleteFriend),
    takeEvery(actions.changeTheme, updatePreferences),
    takeEvery(actions.learnMoreAbout, weather),
    takeEvery(actions.requestFriend, friendMaker),
    takeEvery(actions.loadNextPage, nextPage),
    takeEvery(actions.loadPreviousPage, previousPage),
    takeEvery(actions.loadPreferences, loadPreferences)
  ]);
}