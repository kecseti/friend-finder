import { call, put } from 'redux-saga/effects';
import { actions, url } from '../constants'

export default function* (action){
  const user = yield call(() => fetch(`${url}/users/${action.payload}`).then(res => res.json()))
  const openWeatherUrl = `https://api.openweathermap.org/data/2.5/forecast?q=${user.user.city}&appid=43ade94324a9299fe532a4c005fb71b7&units=metric`
  const weather = yield call(() => fetch(openWeatherUrl).then(res => res.json()) )
  yield put({
    type: actions.learntMore,
    user: user.user,
    weather,
  });
}
