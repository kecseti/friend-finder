import React from 'react';
import { connect } from 'react-redux';
import {succes, failure, information, container, fadeIn, fadeOut} from '../styles/InformationStyle';

class InformationDisplay extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      visible: true
    }
  }
  componentDidMount(){
    this.setState({ visible: false });
  }

  componentDidUpdate(prevProps){
    if(prevProps !== this.props){
      this.setState({ visible: true });
      setTimeout(this.setFadedOut,1500);
    }
  }

  setFadedOut = () => {
    this.setState({ visible: false });
  }
  
  render() {
    const fade = this.state.visible ? fadeIn : fadeOut;
    let status;
    let message;
    if(this.props.statusMessage){
      const resp = this.props.statusMessage.status.split(':')
      if(resp[0].includes('FAILED'))  status =  failure;
      if(resp[0].includes('SUCCESS')) status = succes;
      if(resp[0].includes('INFORMATION')) status = information;
      message = resp[1];
    }
    if(this.props.statusMessage){
      return (
        <div style={{...container, ...status, ...fade}}>
          <h5>{message}</h5>
        </div>
      );
    } else {
      return null;
    }
  }
}

function bindAppStateToProps(appState) {
  return {
    statusMessage: appState.status
  };
}


export default connect(bindAppStateToProps)(InformationDisplay );