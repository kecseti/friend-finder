import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import Button from '@material-ui/core/Button';
import StarBorderOutlinedIcon from '@material-ui/icons/StarBorderOutlined';
import StarIcon from '@material-ui/icons/Star';
import ContactsIcon from '@material-ui/icons/Contacts';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import {listAvatarDesktop, listTextDesktop, buttonContainerDesktop, listAvatarMobile, listTextMobile, buttonContainerMobile, buttonTextMobile} from '../styles/UserListItem';
import { requestFriend, learnMoreAbout, deleteFriend, loadNextPage } from '../actions'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

class UserListItem extends React.Component {
  sendFriendRequest = () => {
    this.props.requestFriend(this.props.id);
  }

  learnMore = () => {
    this.props.learnMoreAbout(this.props.id)
  }

  deleteFriend = () => {
    confirmAlert({
      title: `Break friendship with ${this.props.name}`,
      message: 'Are you sure? You can later add him/her again if you change your mind.',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            this.props.deleteFriend(this.props.id)
            localStorage.setItem('displayStartIndex',0)
            this.props.loadNextPage('friends')
          }
        }
      ]
    });
  }

  render() {
    const avatar = this.props.isPhone === true ? listAvatarMobile : listAvatarDesktop;
    const text = this.props.isPhone === true ? listTextMobile : listTextDesktop;
    const button = this.props.isPhone === true ? buttonContainerMobile : buttonContainerDesktop;
    const buttonText = this.props.isPhone === true ? buttonTextMobile : null;

    const darkModeText = this.props.theme === 'dark'? {color:'white'} : null;

    return (
      <ListItem style={{backgroundColor:`${this.props.backgroundColor}`}}>
        <ListItemAvatar style={avatar}>
          <Avatar src={this.props.photo}/>
        </ListItemAvatar>
        <ListItemText
          primary={this.props.name}
          secondary={
            <Typography variant="caption" > City: {this.props.city}, JobTitle: {this.props.jobTitle} Email: {this.props.email} phone: {this.props.phone} </Typography>
          }
          style={{...text}}
        />
        <div style={button}>
          <Button startIcon = { <ContactsIcon />}  onClick={this.learnMore} style={{...buttonText, ...darkModeText}}>
            Learn more
          </Button>
          <Button startIcon = { this.props.userType === 'friends' ? <StarIcon /> : <StarBorderOutlinedIcon />} onClick = { this.props.userType === 'friends' ? this.deleteFriend : this.sendFriendRequest} style={{...buttonText, ...darkModeText}}>
            {this.props.userType === 'friends' ? 'Delete friend' : 'Add as friend' }
          </Button>
        </div>
      </ListItem>
    );
  }
}
  
function bindAppStateToProps(appState) {
  return {
    theme: appState.theme,
    userType: appState.userType
  };
}

function bindDispatchToProps(dispatch) {
  return bindActionCreators({ requestFriend, learnMoreAbout, deleteFriend, loadNextPage }, dispatch);
}

export default connect(bindAppStateToProps, bindDispatchToProps)(UserListItem);