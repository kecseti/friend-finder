import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ArrowLeftOutlinedIcon from '@material-ui/icons/ArrowLeftOutlined';
import ArrowRightOutlinedIcon from '@material-ui/icons/ArrowRightOutlined';
import ListOutlinedIcon from '@material-ui/icons/ListOutlined';
import AppsOutlinedIcon from '@material-ui/icons/AppsOutlined';
import Button from '@material-ui/core/Button';
import {containerDesktop, containerMobile, buttonMobile, buttonDesktop, previous, next, selectedDisplay, viewButton} from '../styles/ControlStyle';
import { changeMode, loadNextPage, loadPreviousPage } from '../actions'

class CardControls extends React.Component {
  constructor(props){
    super(props);
    this.props.loadNextPage(this.props.userType);
  }
  changeToList = () => {
    this.props.changeMode('list')
  }

  changeToCard = () => {
    this.props.changeMode('card')
  }

  nextPage = () => {
    this.props.loadNextPage(this.props.userType);
  }

  previousPage = () => {
    this.props.loadPreviousPage(this.props.userType);
  }

  render() {
    const container = this.props.isPhone === true ? containerMobile : containerDesktop; 
    const button = this.props.isPhone === true ? buttonMobile : buttonDesktop; 

    const selectedDisplayTheme = this.props.theme === 'dark' ? {...selectedDisplay, ...{backgroundColor:'rgba(218,255,125, 0.25)', color:'white'} } : selectedDisplay;
    const listSelected = this.props.mode === 'list' ? selectedDisplayTheme : null;
    const cardSelected = this.props.mode === 'card' ? selectedDisplayTheme : null;

    const darkModeButton = this.props.theme === 'dark' ? {border:'1px solid #daff7d', color:'white'} : null;
    const darkModeContainer = this.props.theme === 'dark' ? {backgroundColor:'#435058'} : null;
  
    return (
      <div style={{...container, ...darkModeContainer}}>
        <Button startIcon={<ArrowLeftOutlinedIcon />} onClick ={this.previousPage} style={{...button, ...previous, ...darkModeButton}}>
          Previous
        </Button>

        <Button startIcon={<ListOutlinedIcon />} style={{...button, ...listSelected, ...viewButton, ...darkModeButton}} onClick={this.changeToList}>
          List view
        </Button>

        <Button startIcon={<AppsOutlinedIcon />} style={{...button, ...cardSelected, ...viewButton, ...darkModeButton}} onClick={this.changeToCard}>
          Card view
        </Button>

        <Button endIcon={<ArrowRightOutlinedIcon />} onClick ={this.nextPage} style={{...button, ...next, ...darkModeButton}}>
          Next
        </Button>
      </div>
    );
  }
}

function bindAppStateToProps(appState) {
  return {
    theme: appState.theme,
    mode: appState.mode,
    userType: appState.userType
  };
}

function bindDispatchToProps(dispatch) {
  return bindActionCreators({ changeMode, loadNextPage, loadPreviousPage }, dispatch);
}


export default connect(bindAppStateToProps, bindDispatchToProps)(CardControls);