import React from 'react';
import { connect } from 'react-redux';
import UserCard from './UserCard.container';
import UserListItem from './UserListItem.container';
import { cardContainerMobile, cardContainerDesktop,} from '../styles/UserCard'
import { listContainerDesktop, listContainerMobile } from '../styles/UserListItem' 

class UsersDisplay extends React.Component {
  render() {
    const cardContainer = this.props.isPhone === true ? cardContainerMobile : cardContainerDesktop;
    const listContainer = this.props.isPhone === true ? listContainerMobile : listContainerDesktop;
    const container = this.props.mode === 'card' ? cardContainer : listContainer;
    const separateColor = this.props.theme === 'dark' ? 'rgba(218,255,125, 0.1)' : 'rgba(0,0,255,0.05)';
    let toDisplay;
    if(this.props.users){
      const cards = this.props.users.map(user =>{
        if(user) return <UserCard photo = {user.photo} name = {user.name} city = {user.city} jobTitle = {user.jobTitle} email = {user.email} phone = {user.phone} id = {user._id}/>
        else return null;
      });
      const lists = this.props.users.map((user, index) =>{
        if(user)
          return index %2 === 0 ?
            <UserListItem photo = {user.photo} name = {user.name} city = {user.city} jobTitle = {user.jobTitle} email = {user.email} phone = {user.phone} id = {user._id}  isPhone = {this.props.isPhone}/>:
            <UserListItem photo = {user.photo} name = {user.name} city = {user.city} jobTitle = {user.jobTitle} email = {user.email} phone = {user.phone} id = {user._id} isPhone = {this.props.isPhone} backgroundColor = {separateColor}/>
          else return null;
      });
      toDisplay = this.props.mode === 'card' ? cards : lists;
    } else {
      toDisplay = <h1 style={{width: '100%', textAlign: 'center'}}>Ups, We were not able to fulfill your request</h1>
    }

    return (
      <div  style = {container}>
        {toDisplay}
      </div>
    )
  }
}
  
function bindAppStateToProps(appState) {
  return {
    theme: appState.theme,
    mode: appState.mode,
    users: appState.usersToDisplay
  };
}

export default connect(bindAppStateToProps)(UsersDisplay);