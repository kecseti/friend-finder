import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import StarBorderOutlinedIcon from '@material-ui/icons/StarBorderOutlined';
import StarIcon from '@material-ui/icons/Star';
import MoreHorizOutlinedIcon from '@material-ui/icons/MoreHorizOutlined';
import Collapse from '@material-ui/core/Collapse';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import {cardPicture, cardStyle} from '../styles/UserCard';
import { requestFriend, learnMoreAbout, deleteFriend, loadNextPage } from '../actions'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

class UserCard extends React.Component {
  constructor(props){
    super(props);
    this.state={
      expanded: false,
      rotation: 0,
    };
  }

  handleExpand = () => {
    this.setState({ 
      expanded: !this.state.expanded,
      rotation: this.state.expanded === true ? 0 : 180,
    });
  }

  sendFriendRequest = () => {
    this.props.requestFriend(this.props.id);
  }

  deleteFriend = () => {
    confirmAlert({
      title: `Break friendship with ${this.props.name}`,
      message: 'Are you sure? You can later add him/her again if you change your mind.',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            this.props.deleteFriend(this.props.id)
            localStorage.setItem('displayStartIndex',0)
            this.props.loadNextPage('friends')
          }
        }
      ]
    });
  }

  learnMore = () => {
    this.props.learnMoreAbout(this.props.id)
  }

  render() {
    const darkModeCard = this.props.theme === 'dark' ? {color:'white', backgroundColor:'#435058', borderColor:'#daff7d'} : null;
    const darkModeText = this.props.theme === 'dark' ? {color:'white', borderColor:'white'} : null;

    return (
    <Card style = {{...cardStyle, ...darkModeCard}} variant="outlined">
      <CardMedia image={this.props.photo} style = {cardPicture}/>

      <CardHeader
        titleTypographyProps={{ variant:'subtitle1' }}
        title={this.props.name}
        subheader={<Typography variant="caption" >{this.props.city}</Typography>}
        action = {
          <div> 
            <IconButton onClick = {this.props.userType === 'friends' ? this.deleteFriend : this.sendFriendRequest} style={darkModeText}>{ this.props.userType === 'friends' ? <StarIcon /> : <StarBorderOutlinedIcon /> }</IconButton>
            <IconButton 
              onClick={this.handleExpand} 
              aria-label="show more" 
              style={{...{transition: 'all 0.2s ease',transform: `rotate(${this.state.rotation}deg)`},...darkModeText}}> 
                <ExpandMoreIcon /> 
            </IconButton>
          </div>
        }
      />

      <Collapse in={this.state.expanded} timeout="auto" unmountOnExit >
        <div>
          <CardContent style={{paddingTop:0, paddingBottom:0}} >
            <p> Job Title: <b>{this.props.jobTitle}</b><br/>
            Email: <b>{this.props.email}</b><br/>
            Phone number: <b>{this.props.phone}</b></p>
          </CardContent>

          <CardActions style={{paddingTop:0, paddingBottom:2}}>
            <Button onClick={this.learnMore} endIcon={ <MoreHorizOutlinedIcon /> } variant="outlined" style={{...{width:'100%'}, ...darkModeText }}>Learn more</Button>
          </CardActions>
        </div>
      </Collapse>
     
    </Card>
    );
  }
}
  
function bindAppStateToProps(appState) {
  return {
    theme: appState.theme,
    userType: appState.userType
  };
}

function bindDispatchToProps(dispatch) {
  return bindActionCreators({ requestFriend, learnMoreAbout, deleteFriend, loadNextPage }, dispatch);
}

export default connect(bindAppStateToProps, bindDispatchToProps)(UserCard);