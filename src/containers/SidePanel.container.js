import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@material-ui/core/Button';
import AccessibilityNewIcon from '@material-ui/icons/AccessibilityNew';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import AirplayIcon from '@material-ui/icons/Airplay';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import {containerDesktop, buttonDesktop, iconDesktop, descriptionDesktop,
        containerMobile, buttonMobile, iconMobile, descriptionMobile} from '../styles/SidePanelStyle' 
import { changeUserType, loadNextPage } from '../actions'

class SidePanel extends React.Component {
  
  changeToMyFriends = () => {
    this.props.changeUserType('friends')
    localStorage.setItem('displayStartIndex',0)
    this.props.loadNextPage('friends')
  }

  changeToProposeFriends = () => {
    this.props.changeUserType('users');
    localStorage.setItem('displayStartIndex',0)
    this.props.loadNextPage('users')
  }

  render() {
    const container = this.props.isPhone === true ? containerMobile : containerDesktop;
    const icon = this.props.isPhone === true ? iconMobile : iconDesktop;
    const button = this.props.isPhone === true ? buttonMobile : buttonDesktop;
    const description = this.props.isPhone === true ? descriptionMobile : descriptionDesktop;

    const darkModeContainer = this.props.theme === 'dark' ? {borderColor:'#daff7d', backgroundColor:'rgba(218,255,125, 0.08)'} : null;
    const darkModeText = this.props.theme === 'dark' ? {color:'white'} : null;

    const friendsActive = this.props.userType === 'friends'? {backgroundColor: 'rgba(255, 138, 91, 0.2)'} : null;
    const proposeActive = this.props.userType === 'users'  ? {backgroundColor: 'rgba(255, 138, 91, 0.2)'} : null 

    return (
      <div style={{...container, ...darkModeContainer}}>
        <Button startIcon={<AccessibilityNewIcon style={icon} />} onClick={this.changeToMyFriends} style = {{...button, ...darkModeText, ...friendsActive}}>
          My friends
        </Button>

        <Button startIcon={<GroupAddIcon style={icon} />} onClick={this.changeToProposeFriends} style = {{...button, ...darkModeText, ...proposeActive}}>
          Propose friends
        </Button>
        <div style={description}>
          <div style={{padding:10}}>
            <h4>
              This Project was created as a showcase of the learnt subjects on the course held by CodeSpring on the spring of 2020. 
              The course mainly focused on React but it included more, such as modern JavaScript, Redux,Middleware (focused on Saga middleware), 
              React Hooks and Context, NodeJs, MongoDB, Firebase and PWA using React. 
            </h4>
          </div>
          <div> 
            <center>
              <a style={{...{textDecoration:'none'}, ...darkModeText}} href = "https://edu.codespring.ro"> <AirplayIcon /> CodeSpring</a> 
              <a style={{...{textDecoration:'none'}, ...darkModeText}} href = "https://gitlab.com/kecseti"><EmojiPeopleIcon /> Kecseti István </a>
            </center>
          </div>
        </div>
      </div>
    );
  }
}


function bindAppStateToProps(appState) {
  return {
    theme: appState.theme,
    userType: appState.userType
  };
}

function bindDispatchToProps(dispatch) {
  return bindActionCreators({ changeUserType, loadNextPage }, dispatch);
}

export default connect(bindAppStateToProps, bindDispatchToProps)(SidePanel );