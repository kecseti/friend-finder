import React from 'react';
import { connect } from 'react-redux';
import { ClickAwayListener } from '@material-ui/core';
import { Blob } from 'react-blob'
import Chart from '../components/Chart'
import {containerDesktop, containerMobile, dataDisplayMobile, containerHidden, dataDisplayDesktop } from '../styles/WeatherDisplayStyle'

class WeatherDisplay extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      popup: false
    }
  }
  
  handleClickAway = () => {
    this.setState({popup:false});
  }

  componentDidUpdate(prevProps){
    if (prevProps.about !== this.props.about){
      this.setState({ popup: true });
    }
  }

  render() {
    const tempData = [];
    const dateTimeFormat = new Intl.DateTimeFormat('en', { hour12: false, hour: 'numeric',minute: 'numeric', month: 'short', day: '2-digit' }) 
    if(this.props.about){
      this.props.about.weather.list.forEach(element => {
        tempData.push({
          'Temperature': element.main.temp,
          'Humidity': Number(element.main.humidity)/10,
          'date': dateTimeFormat.format(new Date(element.dt_txt))
        })
      })
    } 
    const container = this.props.isPhone === true ? (this.state.popup === true ? containerMobile : containerHidden)  : (this.state.popup === true ? containerDesktop : containerHidden)
    const dataDisplay = this.props.isPhone === true ? dataDisplayMobile : dataDisplayDesktop;
    const blobSize = this.props.isPhone === true ? '90px' : '160px';
    const width = this.props.isPhone === true ? 350 : 900;
    const height = this.props.isPhone === true ? 170 : 250;
    const darkModeContainer = this.props.theme === 'dark' ? {borderColor:'#daff7d', backgroundColor:'#435058'} : null;
    const darkModeDataDisplay = this.props.theme === 'dark' ? {color:'black'} : null;
    if(this.props.about){
      return (
        <ClickAwayListener onClickAway={this.handleClickAway}>
          <div style={{...container, ...darkModeContainer}} >
            <Blob size={blobSize} 
                  src={this.props.about.user.photo}
                  style = {{marginLeft:'5%', marginTop:'2%'}} />
            
            <div style = {{marginLeft:'3%', marginTop:'3%'}}>
              <h2>{this.props.about.user.name}</h2>
              <p>City: <b>{this.props.about.user.city}</b></p>
              <p>Job Title: <b>{this.props.about.user.jobTitle}</b></p>
              <p>Email: <b>{this.props.about.user.email}</b></p>
              <p>Phone number: <b>{this.props.about.user.phone}</b></p>
            </div>

            <div style = {{...dataDisplay, ...darkModeDataDisplay}} > 
              <Chart data={tempData} width = {width} height = {height}/>
            </div>
          </div>
        </ClickAwayListener>
      );
    } else {
      return(<div></div>)
    }
  }
}

function bindAppStateToProps(appState) {
  return {
    theme: appState.theme,
    about: appState.weather
  };
}

export default connect(bindAppStateToProps)(WeatherDisplay);