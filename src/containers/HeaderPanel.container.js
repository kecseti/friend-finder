import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Typography from '@material-ui/core/Typography';
import MenuOutlinedIcon from '@material-ui/icons/MenuOutlined';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {headerContainerDesktop, headerContainerMobile, menuDesktop, menuMobile, logoMobile, logoDesktop} from '../styles/HeaderStyle'
import { changeTheme, loadPreferences } from '../actions'

class HeaderPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      anchor: null
    }
  }
  componentDidMount() {
    this.props.loadPreferences()
  }
  handleOpen = (event) => {
    this.setState({ anchor: event.currentTarget});
  }
  handleClose = () => {
    this.setState({ anchor: null});
  }

  switchTheme = () => {
    this.setState({ anchor: null});
    this.props.changeTheme(this.props.theme === 'light' ? 'dark' : 'light');
  }

  render() {
    this.props.theme === 'dark' ? document.body.style = 'background: #435058; color: white;' : document.body.style = 'background: white; color: black;'
    const headerContainer = this.props.isPhone === true ? headerContainerMobile : headerContainerDesktop; 
    const menu = this.props.isPhone === true ? menuMobile : menuDesktop; 
    const logo = this.props.isPhone === true ? logoMobile : logoDesktop; 

    // Dark mode, TODO separate to style file
    const darkModeContainer = this.props.theme === 'dark' ? {backgroundColor:'#435058', borderColor:'#daff7d'} : null;
    const darkModeIcon = this.props.theme === 'dark' ? {color: 'white'} : null;

    return (
      <div style = {{...headerContainer, ...darkModeContainer}}>
        <IconButton onClick={this.handleOpen} style={menu}> 
          <MenuOutlinedIcon fontSize='large' style = {darkModeIcon}/> 
        </IconButton>
        <Menu anchorEl={this.state.anchor} keepMounted open={Boolean(this.state.anchor)} onClose={this.handleClose} >
          <MenuItem onClick={this.switchTheme}>Switch Theme</MenuItem>
          <MenuItem onClick={this.handleClose}>Logout</MenuItem>
        </Menu>
        <Typography variant="h5" align="center" noWrap>
          Friender
          <img src={this.props.theme === 'dark' ? "green_logo.png" : "blue_logo.png"} style={logo} alt="Friender logo"/>
        </Typography>
       
      </div>
    );
  }
}

function bindAppStateToProps(appState) {
  return {
    theme: appState.theme
  };
}

function bindDispatchToProps(dispatch) {
  return bindActionCreators({ changeTheme, loadPreferences }, dispatch);
}


export default connect(bindAppStateToProps, bindDispatchToProps)(HeaderPanel);