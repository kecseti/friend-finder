/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
self.addEventListener('install', event => {
  console.log('install *****')
})
self.addEventListener('activate', event => {
  console.log('activate *****')
})

// --- 2 ----
//console.log("Hello World with config-overrided")
//
// run teh workbox created sw in  debbug modus or production modus 
workbox.setConfig({debug: true}); 

// no waiting 
workbox.core.skipWaiting();

// if older version are running in separete tabs 
workbox.core.clientsClaim();

// --- 3 ---

 


//workbox-precaching does all of this during the service worker's install event.
// The response strategy used in this route is cache-first: 
// the precached response will be used, unless 
// that cached response is not present (due to some unexpected error),
// in which case a network response will be used instead.

 

workbox.precaching.precacheAndRoute(self.__precacheManifest || []);

// --- 4 ----
//

 

// Resources are requested from both the cache and the network in parallel. 
// The strategy will respond with the cached version if available,
// otherwise wait for the network response.
// The cache is updated with the network response with each successful request.
workbox.routing.registerRoute(
  new RegExp('/.*'),
  new workbox.strategies.StaleWhileRevalidate()
);

// --- 5 ----

 

// set the push notification 
self.addEventListener('push', (event) => {
  const data = event.data.json()
  console.log('New notification', data)
  const options = {
    body: data.body,
  }
  event.waitUntil(
    self.registration.showNotification(data.title, options)
  );

});