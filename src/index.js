import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import allReducers from './reducers';

import rootSaga from './sagas';
import createSagaMiddleware from 'redux-saga';

import * as serviceWorker from './serviceWorker';

const sagaMiddleware = createSagaMiddleware();

const store = createStore( allReducers, applyMiddleware(sagaMiddleware) ); 
sagaMiddleware.run(rootSaga);

localStorage.setItem('displayStartIndex', 0);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root'));

serviceWorker.register();
