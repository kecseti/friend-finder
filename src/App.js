import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import MainPage from './components/MainPage'
import LoginPage from './components/LoginPage'

import './styles/base.css';

function App() {
  return (
    <BrowserRouter >
      <Route path="/" exact component = {LoginPage} />
      <Route path="/main" exact component={MainPage} />
    </BrowserRouter>
    
  );
}

export default App;
