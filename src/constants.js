const localURL = 'http://localhost:3000';
const productionURL = null;

export const url = productionURL || localURL;

export const actions = {
  changeTheme : 'UPDATE_THEME_REQUEST',
  themeChanged: 'THEME_CHANGED',
  changeMode : 'UPDATE_MODE',
  loadNextPage: 'LOAD_NEXT_PAGE_FROM_API',
  loadPreviousPage: 'LOAD_PREVIOUS_PAGE_FROM_API',
  loadNextPageSucces: 'LOAD_NEXT_PAGE_FROM_API_SUCCES',
  loadNextPageFailure: 'LOAD_NEXT_PAGE_FROM_API_FAILURE',
  loadPreviousPageSucces: 'LOAD_PREVIOUS_PAGE_FROM_API_SUCCES',
  loadPreviousPageFailure: 'LOAD_PREVIOUS_PAGE_FROM_API_FAILURE',
  friendshipMade: 'FRIENDSHIP_MADE_SUCCES',
  requestFriend: 'FRIENSHIP_REQUEST',
  learnMoreAbout: 'LEARN_MORE_ABOUT_FROM_API',
  learntMore: 'LEARNT_MORE_ABOUT',
  changeUserType: 'CHANGE_USER_TYPE',
  deleteFriend: 'DELETE_FRIEND',
  friendDeleted: 'DELETE_FRIEND_SUCCESS',
  loadPreferences: 'INITIAL_LOAD_ON_PREFERENCES'
}