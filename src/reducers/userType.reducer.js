import { actions } from '../constants'

export default function (appState = null, action) {
  switch (action.type) {
    case actions.changeUserType:
      return action.payload;
    default:
      return appState || 'users';
  }
}