import usersToDisplay from './users.reducer';
import weather from './weather.reducer'
import theme from './theme.reducer'
import status from './status.reducer'
import userType from './userType.reducer'
import mode from './mode.reducer'

import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  usersToDisplay,
  theme,
  weather,
  mode,
  status,
  userType,
});

export default rootReducer;
