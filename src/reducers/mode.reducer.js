import { actions } from '../constants'

export default function (appState = null, action) {
  switch (action.type) {
    case actions.changeMode:
      return action.payload;
    default:
      return appState || 'list';
  }
}