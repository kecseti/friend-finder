import { actions } from '../constants'

export default function (appState = null, action) {
  switch (action.type) {
    case actions.learntMore:
      return {user: action.user, weather: action.weather};
    default:
      return appState;
  }
}