import { actions } from '../constants'

export default function (appState = null, action) {
  switch (action.type) {
    case actions.loadNextPageSucces:
    case actions.loadPreviousPageSucces:
      return action.payload.users
    default:
      return appState;
  }
}