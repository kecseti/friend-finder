import { actions } from '../constants'

export default function (appState = null, action) {
  switch (action.type) {
    case actions.friendDeleted:
    case actions.friendshipMade:
      return action.payload;
    default:
      return appState;
  }
}