import { actions } from '../constants'

export default function (appState = null, action) {
  switch (action.type) {
    case actions.themeChanged:
      console.log(action)
      return action.payload;
    default:
      return appState || 'light';
  }
}