export const cardContainerMobile ={
  display: 'flex', 
  flexWrap: 'wrap', 
  justifyContent:'center', 
  alignItems: 'flex-start', 
  width:'100%',
  paddingTop:50
}

export const cardContainerDesktop ={
  display: 'flex', 
  flexWrap: 'wrap', 
  justifyContent:'center', 
  alignItems: 'flex-start', 
  width:'80%',
  float:'right',
  marginLeft:0,
  marginRight:0,
  paddingLeft:0,
  paddingRight:0,
}

export const cardStyle = {
  width:270, 
  minHeight: 260,
  margin:10
}

export const cardPicture = {
  height: 0,
  paddingTop: '56.25%'
}