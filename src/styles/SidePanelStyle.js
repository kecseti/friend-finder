export const containerDesktop = {
  display:'flex',
  flexDirection: 'column',
  backgroundColor:'rgba(0,0,255,0.05)', 
  width:'19%',
  height:'91.5vh', 
  position:'absolute',
  top:80,
  left:0,
  borderRight:'1px solid blue',
  outlineOffset:' -2px'
}

export const containerMobile = {
  display:'flex',
  flexDirection:'column',
  position:'fixed',
  top:0,
  left:0,
  zIndex:1300
}

export const buttonDesktop = {
  width:'100%',
  height:'100px',
}

export const buttonMobile = {
  height:'25px',
  fontSize: '0',
  zIndex:1300
}

export const iconDesktop = {
  fontSize: '40px'
}

export const iconMobile = {
  fontSize: '25px'
}

export const descriptionDesktop = {
  marginTop:'auto'
}

export const descriptionMobile = {
  display:'none',
}