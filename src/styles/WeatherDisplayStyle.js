export const containerDesktop = {
  width:'100%',
  height:'30%',
  position:'absolute',
  bottom:0, 
  right:0, 
  display:'flex',
  borderTop: '2px solid blue',
  boxShadow: '0 0 0 100vmax rgba(0,0,0,.6)',
  zIndex: 1301,
  backgroundColor:'white',
}

export const containerMobile = {
  width:'100%',
  height:'500',
  position:'fixed',
  bottom:0, 
  right:0, 
  display:'flex',
  flexDirection:'column',
  borderTop: '2px solid blue',
  backgroundColor:'white',
  boxShadow: '0 0 0 100vmax rgba(0,0,0,.6)',
  zIndex: 1301
}



export const dataDisplayDesktop = {
  height:'95%',
  marginLeft:'3%', 
  width:'60%'
}

export const dataDisplayMobile = {
  height:175,
  margin:0, 
  width:'100%'
}

export const containerHidden = {
  display:'none',
}
