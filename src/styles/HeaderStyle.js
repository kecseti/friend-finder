export const headerContainerDesktop = {
  width:'100%',
  height:'8vh',
  borderBottom: '2px solid blue',
  marginBottom: 2
}

export const headerContainerMobile = {
  width:'100%',
  height:50,
  borderBottom: '2px solid blue',
  marginBottom: 2,
  position:'fixed',
  top:0,
  backgroundColor:'white',
  zIndex:1300
}

export const menuDesktop = {
  position: 'absolute',
  top:20, 
  right:10
}

export const menuMobile = {
  position: 'absolute',
  top:0, 
  right:0
}

export const logoDesktop = {
  height: 50,
  marginLeft:"5%",
  marginTop:'5px',
}

export const logoMobile = {
  height: 30,
  marginLeft:"5%",
  marginTop:'5px', 
}