export const containerDesktop = {
  width:'80%',
  height: 30,
  float:'right',
  display:'flex',
  padding:5
}

export const containerMobile = {
  width:'100%',
  height: 30,
  float: 'right',
  display:'flex',
  position:'fixed',
  backgroundColor: 'white',
  bottom: 0,
  margin:0,
  borderTop:'1px blue solid',
  padding: 3,
  zIndex:1300
}

export const buttonDesktop = {
  backgroundColor:'transparent',
  width:'200px',
}

export const buttonMobile = {
  backgroundColor:'transparent',
  width:'100px',
  fontSize:'0.5rem',
}

export const viewButton = {
  transition: 'background-color 500ms linear'
}

export const previous = {
  marginRight:'auto', 
  marginLeft:'3%',
  border:'1px solid blue',
}

export const next = {
  marginLeft:'auto',
  marginRight:'3%',
  border:'1px solid blue',
}

export const selectedDisplay = {
  backgroundColor:'rgba(0,0,255,0.2)'
}
