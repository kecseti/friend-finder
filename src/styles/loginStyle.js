export const container = {
  width: '100vw',
  height: '100vh',
  background: 'url(background.jpg)',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
}

export const flexbox = {
  position: 'absolute',
  width:'100%',
  transform: 'translate(-50%, -50%)',
  top:'50%',
  left:'50%',
  color: 'white',
  dispaly:'flex',
  flexDirection:'row',
  justifyContent:'center',
}

export const title = {
  fontWeight:'750',
  WebkitTextStrokeWidth: '2px', 
  WebkitTextStrokeColor:'#242038',
  marginLeft: '3%', 
  display: 'inline-block', 
  color:'#5b7db5', 
  transform: 'translate(0%, 70%)'
}

export const button = {
  marginLeft: '50%', 
  display: 'inline-block'
}