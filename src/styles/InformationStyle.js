export const container ={
  position: 'absolute',
  bottom: 30,
  transform: 'translate(-50%, -50%)',
  left:'50%',
  padding: 10, 
  borderRadius: 5,
  transition:'opacity 3s ease-in-out'
}

export const failure = {
  backgroundColor:'red',
  color:'white'
}

export const succes = {
  background: 'green',
  color:'white',
}

export const information = {
  backgroundColor:'orange',
  color:'black'
}

export const fadeOut = {
  opacity:0,
  transition: 'opacity 0.5s ease-in-out'

}

export const fadeIn = {
  opacity:1,
  transition: 'opacity 0.5s ease-in-out'

}