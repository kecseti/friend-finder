export const listContainerMobile ={
  display: 'flex', 
  flexDirection: 'column', 
  justifyContent:'center', 
  alignItems: 'flex-start', 
  width:'100%',
  paddingTop:50
}
export const listContainerDesktop ={
  display: 'flex', 
  flexDirection: 'column', 
  justifyContent:'center', 
  alignItems: 'flex-start', 
  width:'80%',
  float:'right',
}
export const listAvatarDesktop ={
  marginLeft:'10%'
} 
export const listTextDesktop ={
  marginLeft:'3%'
} 
export const buttonContainerDesktop ={
  marginRight:'10%'
} 
export const listAvatarMobile ={
  margin:0,
} 
export const listTextMobile ={
  margin:0,
} 
export const buttonContainerMobile={
  marginRight:0,
}
export const buttonTextMobile = {
  fontSize: 0,
}