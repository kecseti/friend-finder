import React from 'react';
import HeaderPanel from '../containers/HeaderPanel.container';
import CardControls from '../containers/CardControls.container';
import SidePanel from '../containers/SidePanel.container';
import WeatherDisplay from '../containers/WeatherDisplay.container';
import UsersDisplay from '../containers/UsersDisplay.container';
import InformationDisplay from '../containers/InformationDisplay.container';
import Cookies from 'js-cookie';
import { useMediaQuery } from 'react-responsive'
import { useEffect } from 'react';

function MainPage() {
  useEffect(() => {
    localStorage.setItem('token', `Bearer: ${Cookies.get('token')}`);
  })
  const isPhone = useMediaQuery({ maxWidth:  800});
  
  return (
      <div>
        <HeaderPanel isPhone = {isPhone} />
        <CardControls isPhone = {isPhone} />
        <UsersDisplay isPhone = {isPhone} />
        <SidePanel isPhone = {isPhone} />
        <InformationDisplay />
        <WeatherDisplay isPhone = {isPhone} />
      </div>
                                         
    
  );
}

export default MainPage;
