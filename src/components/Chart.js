import { CartesianGrid, ComposedChart, Bar, Area, XAxis, Tooltip } from 'recharts';
import React from 'react'

// http://recharts.org/en-US
export default ({ data, width, height }) => {
    console.log(data);
    return (
    <ComposedChart width={width} height={height} data={data}>
        <Area type="monotone" dataKey="Temperature" stroke="red" fill='#eba78a' activeDot={{ r: 8 }}  />
        <Bar type="monotone" dataKey="Humidity" fill="#8884d8"/>
        <XAxis dataKey="date" interval="preserveStartEnd"/>
        <Tooltip />
        <CartesianGrid strokeDasharray="3 3" />
    </ComposedChart>
);}