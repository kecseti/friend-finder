import React from 'react';
import { container, flexbox, title, button } from '../styles/loginStyle'
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import FacebookIcon from '@material-ui/icons/Facebook';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import { url } from '../constants'

const IconButtonFacebook = withStyles({
  root: {
    '&:hover': {
      boxShadow: '0px 0px 101px 3px rgba(168, 0, 163,0.74)',
      transition:'box-shadow 0.2s ease-in-out',
    }
  }
})(IconButton);

export default class LoginPage extends React.Component {

  loginWithFb = () => {
    window.open(`${url}/auth/facebook`, `_self`);
  }

  render() {
    return (
      <div style = { container }>
        <div style = { flexbox }>
            <Typography variant="h1" align="center" noWrap style={ title }> Friender </Typography>
            <div style={ button }>
              <Tooltip title="Log in with facebook!"  >
                <IconButtonFacebook onClick={this.loginWithFb}> 
                  <FacebookIcon style={{fontSize:300, color:'#5b7db5'}}/> 
                </IconButtonFacebook>
              </Tooltip>
            </div>
        </div>
       
      </div>
    );
  }
}