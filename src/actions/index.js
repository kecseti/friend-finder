import { actions } from '../constants'

export function getWeather(city){
  return {
    type: actions.getWeatherFromAPI,
    payload: city
  }
}

export function changeTheme(theme){
  return {
    type: actions.changeTheme,
    payload: theme
  }
}

export function changeMode(mode){
  return {
    type: actions.changeMode,
    payload: mode
  }
}

export function loadNextPage(userType){
  return {
    type: actions.loadNextPage,
    payload:userType,
  }
}

export function loadPreviousPage(userType){
  return {
    type: actions.loadPreviousPage,
    payload:userType,
  }
}

export function requestFriend(id){
  return {
    type: actions.requestFriend,
    payload: id
  }
}

export function deleteFriend(id){
  return {
    type: actions.deleteFriend,
    payload: id
  }
}

export function learnMoreAbout(id){
  return {
    type: actions.learnMoreAbout,
    payload: id
  }
}

export function changeUserType(userType){
  return {
    type: actions.changeUserType,
    payload: userType
  }
}

export function loadPreferences(){
  return {
    type: actions.loadPreferences,
  }
}
