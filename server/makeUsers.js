const Faker = require('faker');
const userMongoDB = require ('./models/users.mongoose'); 

const possibleCitiesKnownToOpenWeather = ['Budapest', 'Cluj-Napoca', 'London', 'Paris','Oradea','Gheorgheni' ];

Array.from(new Array(100)).forEach(()=>{
  userMongoDB.create(
    Faker.image.avatar(),
    Faker.name.firstName() + ' ' + Faker.name.lastName(),
    possibleCitiesKnownToOpenWeather[Math.floor(Math.random() * possibleCitiesKnownToOpenWeather.length)],
    Faker.name.jobTitle(), 
    Faker.internet.email(), 
    Faker.phone.phoneNumber('074########'), 
    [], 
    'light', 
    'english'
    )
});
