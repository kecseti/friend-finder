const express = require('express');
const cors = require('cors')
const userMongoDB = require ('./models/users.mongoose'); 
const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const session = require('express-session');
const secret = require('./psszt')
const app = express();
const jwt = require('jsonwebtoken');

const preferences = require('./routes/preferences.js');
const friends = require('./routes/friends.js');
const users = require('./routes/users.js');

passport.serializeUser((user, done) => { done(null, user); });
passport.deserializeUser((obj, done) => { done(null, obj); });
let token;

passport.use(new FacebookStrategy({ 
    clientID: secret.facebookApiKey,
    clientSecret:secret.facebookApiSecret ,
    callbackURL: secret.facebookCallbackUrl,
    profileFields: [ 'location', 'email', 'first_name','last_name', 'picture.type(large)', 'link', ]
  },
  async (accessToken, refreshToken, profile, done) => {
    let user = await userMongoDB.readByEmail(profile._json.email)
    if(!user){
      await userMongoDB.create(profile._json.picture.data.url,
                        `${profile._json.first_name} ${profile._json.last_name}`,
                        profile._json.location.name, 
                        'Developer',
                        profile._json.email,
                        789654123, 
                        [], 'light', 'english')
    } user = await userMongoDB.readByEmail(profile._json.email)
    token = await jwt.sign({ id: user._id }, secret.jwtSecret)
    return done(null,user);
  }
));

app.set('PORT', process.env.PORT || 3000);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(session({ secret: 'CrazyCatsEye', key: 'Gooorangers'}));
app.use(passport.initialize());
app.use(passport.session());

app.get('/auth/facebook', passport.authenticate('facebook',{scope:['email', 'public_profile', 'user_location']}));

app.get('/auth/facebook/callback', passport.authenticate('facebook', {failureRedirect: '/' }), async (_, response) => {
  response.cookie('token', token);
  response.redirect(secret.clientUrl);
});

app.get('/logout', (req, res) => { 
  req.logout();
  res.json({ status:'Logged out'});
});

app.use('/users', users);
app.use('/friends', friends);
app.use('/preferences', preferences);

app.listen(app.get('PORT'), () => console.log(`Server running on port ${app.get('PORT')}`));
