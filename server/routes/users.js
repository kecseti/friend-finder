const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const debug = require('debug')('server:preferences.js')
const { check, validationResult } = require('express-validator');
const userMongoDB = require ('../models/users.mongoose'); 
const secret = require('../psszt')

const app = express.Router();
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));

app.get('/:id?/:top?', async (request, response) => {
  if(request.params.id && request.params.top){
    const bottom = request.params.id;
    const { top } = request.params;
    userMongoDB.readAllBetween(Number(bottom),Number(top))
      .then((users) => {
        debug(`${bottom} -- ${top}`)
        debug(`Found users: ${users.length}`);
        response.status(200).json({status:'SUCCESS: Found', users});
      })
      .catch((err) =>{
        debug(`Users between ${bottom} and ${top} was not found: ${err}`);
        response.status(404).json({status:'FAILED: Not found or index out of bound'});
      }) 
  } else if (request.params.id && !request.params.top){
    if(request.params.id === 'my'){
      debug(`Request to get the user holding a token.`);
      const userId = await jwt.decode(request.headers.authorization.replace('Bearer: ', ''), secret.jwtSecret).id
      userMongoDB.readById(userId)
        .then((user) =>{
          debug(`Found user: ${user}`);
          response.status(200).json({status:'SUCCESS: Found', user});
        })
        .catch((err) =>{
          debug(`Userholding token: ${request.headers.authorization} was not found`);
          response.status(404).json({status:'FAILED: Not found'});
        })
    } else {
      debug(`Search for user by id: ${request.params.id}`);
      userMongoDB.readById(request.params.id)
        .then((found) => {
          debug(`Found user: ${found}`);
          response.status(200).json({status:'SUCCESS: Found', user: found});
        })
        .catch((err) =>{
          debug(`User with id: ${request.params.id} was not found`);
          response.status(404).json({status:'FAILED: Not found'});
        })
    } 
  } else {
      response.status(422).json({status:'FAILED: No parameter given, please provide the id or a top-bottom index'});
  }  
});

app.post('/',[ 
check('photo').isString(), 
check('name').isString(), 
check('city').isString(), 
check('jobTitle').isString(),
check('email').isEmail(),
check('phone').isNumeric()
], (request, response) => {
  debug(request.body);
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    debug(errors.array());
    response.status(422).json({status:'FAILED: Bad request', errors: errors.array() })
  }
  userMongoDB.create(request.body.photo, request.body.name, request.body.city, request.body.jobTitle, request.body.email, request.body.phone, [], 'light', 'english')
  .then((created) => {
    debug(`Persisted: ${created}`);
    response.status(201).json({status:'SUCCESS: Persisted', id: created._id});
  })
  .catch((error) =>{
    debug(`Ups, Error on creating object in database: ${error}`)
    response.status(500).json({status:'FAILED: Cannot persist object'});
  })
});

module.exports = app;

