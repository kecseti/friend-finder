const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const debug = require('debug')('server:preferences.js')
const userMongoDB = require ('../models/users.mongoose'); 
const secret = require('../psszt')

const app = express.Router();
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));

app.post('/', async (request, response) => {
  debug(request.body);
  const userId = await jwt.decode(request.headers.authorization.replace('Bearer: ', ''), secret.jwtSecret).id
  userMongoDB.updatePreferences(request.body.language, request.body.theme, userId)
    .then((user) =>{
      debug(`Updated users preferences`)
      response.status(200).json({status:'INFORMATION: Updated', user});
    })
    .catch((err) =>{
      debug(`Ups, Error on updating preferences to`)
      response.status(500).json({status:'FAILED: Updating has failed'});
    })
})

module.exports = app;
