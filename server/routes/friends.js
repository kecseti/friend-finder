const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');
const debug = require('debug')('server:friends.js')
const userMongoDB = require ('../models/users.mongoose'); 
const secret = require('../psszt')

const app = express.Router();
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));

app.get('/:lower/:upper/',async (request, response) => {
  const { lower, upper} = request.params;
  debug(`${lower} -- ${upper}`)
  const userId = await jwt.decode(request.headers.authorization.replace('Bearer: ', ''), secret.jwtSecret).id
  userMongoDB.findFriendsBetween(Number(lower),Number(upper),userId)
    .then((users) => {
      debug(`Found friends: ${users.length}`);
      response.status(200).json({status:'SUCCESS: Found', users});
    })
    .catch((err) =>{
      debug(`Friends between ${lower} and ${upper} was not found: ${err}`);
      response.status(404).json({status:'FAILED: Not found or index out of bound'});
    }) 
})

app.post('/',[
  check('userToAddAsFriendId').isAlphanumeric()
  ], async (request, response) => {
  debug(request.body);
  const userToAddFriendToId = await jwt.decode(request.headers.authorization.replace('Bearer: ', ''), secret.jwtSecret).id
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    debug(errors.array());
    response.status(422).json({status:'FAILED: Bad request', errors: errors.array() })
  }
  const thisUser = await userMongoDB.readById(userToAddFriendToId);
  if(thisUser.friends.includes(request.body.userToAddAsFriendId)){
      debug(`Friend added to the user with ID: ${userToAddFriendToId}`)
      response.status(200).json({status:'INFORMATION: Already friend'});
  } else {
    userMongoDB.addFriend(userToAddFriendToId, request.body.userToAddAsFriendId)
    .then(() =>{
      debug(`Friend added to the user with ID: ${userToAddFriendToId}`)
      response.status(200).json({status:'SUCCESS: Friend added successfully'});
    })
    .catch((err) =>{
      debug(`Ups, Error on adding friends to : ${userToAddFriendToId} :${err}`)
      response.status(500).json({status:'FAILED: adding friend has failed'});
    })
  }
})

app.delete('/',[
  check('userToDeleteAsFriendId').isAlphanumeric()
  ], async (request, response) => {
  debug(request.body);
  const userToDeleteFriendFromId = await jwt.decode(request.headers.authorization.replace('Bearer: ', ''), secret.jwtSecret).id
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    debug(errors.array());
    response.status(422).json({status:'FAILED: Bad request', errors: errors.array() })
  }
  userMongoDB.deleteFriend(userToDeleteFriendFromId, request.body.userToDeleteAsFriendId)
    .then(() =>{
      debug(`Friend deleted from the user with ID: ${userToDeleteFriendFromId}`)
      response.status(200).json({status:'SUCCESS: Friend deleted successfully'});
    })
    .catch((err) =>{
      debug(`Ups, Error on deleting friends from : ${userToDeleteFriendFromId} :${err}`)
      response.status(500).json({status:'FAILED: deleting friend has failed'});
    })
})

module.exports = app;
