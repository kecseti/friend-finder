const debug = require('debug')('server:users.schema'); 
const mongoose  = require('mongoose'); 
const secrets = require('../psszt');
const Schema = mongoose.Schema; 

const connectionString=secrets.connectionString

mongoose.connect(connectionString, { useNewUrlParser: true } ).catch ((error) => { debug(`Ups, cannot connect to database because reasons: ${error}`); }); 
mongoose.connection.on('error', (error)  => { debug(`Ups, something went wrong during operation with db because reasons: ${error}`); });

mongoose.set('useFindAndModify', false);

const userSchema = new Schema ({ 
                            photo: String,
                            name : String,
                            city : String,
                            jobTitle : String,
                            email : String,
                            phone : Number,
                            friends : Array,
                            theme: String,
                            language: String,
                        }); 

module.exports = mongoose.model('users', userSchema);
