const UserModel = require('./users.schema')
const debug = require('debug')('server:users.mongoose.js'); 

async function readById(ObjectId) { 
    if(ObjectId !== undefined){
      const result = await UserModel.findOne({_id: ObjectId}); 
      if (!result) {
        debug(`Cannot  read from  db an object ${ObjectId}  `)
        return null;
      } else {
        debug(`Just read from the db an object ${result.name}`)
        return result;  
      }   
    }
   else return null
}

async function readByEmail(email) { 
  if(email !== undefined){
    const result = await UserModel.findOne({email: email}); 
    if (!result) {
      debug(`Cannot  read from  db an object ${email}  `)
      return null;
    } else {
      debug(`Just read from the db an object ${result.name}`)
      return result;  
    }   
  }
 else return null
}

async function readAllBetween(lower, upper) { 
  const result = await UserModel.find().limit(upper-lower).skip(lower).exec()
    if (!result) {
      debug(`Cannot  read from  db objects between ${lower} and ${upper}`)
      return null;
    } else {
      debug(`Just read from the db objects`)
      return result;  
    }   
}

async function create(photo, name, city, jobTitle, email, phone, friends, theme, language) { 
    const newUserDocument =  new UserModel({ photo, name, city, jobTitle, email, phone, friends, theme, language }); 
    const resultedDoc = await newUserDocument.save()
    debug(`Just created in the  db an object ${resultedDoc}`)
    return resultedDoc; 
}

async function addFriend(myUser, friendToAdd) { 
  debug(`Friend added to the user ${myUser}`)
  UserModel.findByIdAndUpdate( myUser, { $addToSet: { friends: friendToAdd } }).exec(); 
}

async function deleteFriend(myUser, friendToDelete) { 
  debug(`Friend deleted from the user ${myUser}`)
  UserModel.findByIdAndUpdate( myUser, { $pull: {friends: friendToDelete } }).exec(); 
}

async function findFriendsBetween(lower, upper, myUser){
  const userToFindFriendsFor = await readById(myUser);
  const friends = [];
  let friendIndex = lower-1;
  let found;
  while(Number(++friendIndex) <= Number(lower) + Number(upper)){
    if((found = await readById(userToFindFriendsFor.friends[friendIndex])) !== null)
      friends.push(found)
  }
  debug(`Friends found for the user ${myUser} `)
  return friends
}

async function updatePreferences(language, theme, id){
  language = language || 'english';
  theme = theme || 'light';
  debug(`theme: ${theme} & language: ${language}`);
  await UserModel.findByIdAndUpdate( id, { language: language, theme: theme}).exec();
  debug(`Preferences updated for user: ${id}`)
  return readById(id);
}

module.exports = { create, readById, readAllBetween, addFriend, deleteFriend, findFriendsBetween, updatePreferences, readByEmail }

