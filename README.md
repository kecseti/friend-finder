# Friender
  This section contains the changes before the CodeSpring git is set up <br />
  Kind of a changelog

# Startup
  Ahhoz hogy applikációd futása sikeres legyen <br />
  Vedd észre a két .bat fájlt, itt ezen a helyen. <br />
  Először, csak hogy mindenki boldogan távozzon, <br />
  Legyen telepítve npm, ha nincs, ne engem vádoljon... <br />

  Most következik a fő attrakció, amire mindenki vár, <br />
  A 'setup.bat'-ra duplaklikk, ez telepít, nem humorizál. <br />
  Amint a két fekete ablak eltűnt és nem hibával elmenekült <br />
  Biztos lehetsz benne, hogy minden csomag települt.

  Minden készen, minden rendben, eddig a pontig, <br />
  A 'startup.bat'-ra dupla bal és nem pedig jobbklikk. <br />
  Két fekete ablak kell felugorjon, benne rengeteg szöveg <br />
  Siker után, fut a szervered és a kliensed kisöreg!  <br />

  ```
  1. Install npm on your machine
  2. Double click on setup.bat
  3. After setup has finished, double click on startup.bat
  ```

### Features of the client
  - Added component to display header bar
  - Named the application Friender from Friend Finder, created two logo
  - Added dropdown menu on the header bar
  - Created side navigation bar with a little description about the project and with two buttons as required in the pdf
  - Created a display panel to display the cards or the list
  - Created card items, shows a picture, name and city and two buttons, one to add as friend, and one to extend card
  - Added feature to the card to extend revealing more information about user: job title, email, phone number and a button to learn more about user
  - Created controls for the panel mentioned above, containing 4 buttons, previous, next and two button functions as radio button, to switch between list and card view
  - Added the list view item, containing a little avatar picture, every information about a user, and two buttons, one to add as friend and one to learn more
  - Added a component witch contains a bubbling avatar image, all the information about a user and a Chart showing the temperature and humidity on his/her town
  - Added icons to buttons (It's beautiful now, mommy!)
  - Implemented Dark mode feature, based on a Boolean variable, now can switch between dark and light mode
  - Implemented dark mode for all the components mentioned above (Mommy, it's not only beautiful, but cool with gray and neon!)
  - Figured out that the application does not work on mobile/tablet mode so decided to implement that one too
  - Implemented mobile mode for all the components mentioned above
  - Implemented a media query based on witch can switch between mobile and desktop mode
  - Extended dark mode to support mobile view
  - Implemented a pop-up feature for the temperature display panel on mobile devices
  - Created actions for get users from db, to post a friendship to delete a friendship, to get weather, to change theme, to change display mode, to load next and     previous pages and to get more information about a user.
  - Created a constants.js file to store the action strings and the url and all sort of color related things for the themes
  - Added reducers and created the application state
  - Added saga middlewares for all the above mentioned where communication with backend was necessary
  - Created a change userType action to change between users and friends
  - Created containers from components
  - Changed the chart where the weather displays, now it is nice, I think
  - Connected up all the components required from client side to either the application state or to the server
  - Added a login page with a login with facebook button on it
  - Added login with facebook functionality
  - Made PWA from the application, added caching, installing

### Features of the server
  - Created schema for a user with mongoose
  - Connected with mongo DB (Hurray!)
  - Created 6 end points, to get users, to get friends, to post users, to make friendships, to break friendships, to update preferences of a user
  - Created function on mongoose model to use inside these endpoints
  - Implemented getting a user between two index values and also by id, implemented it for friend as well, but there can not search by ID.
  - Implemented posting a user
  - Implemented posting a friendships and deleting one
  - Implemented changing the preferences of a user
  - Added error handling for all the endpoints, now it send back correct http status codes
  - Introduced a status message with every response
  - Added check of the incoming objects, to make sure they are in the correct format
  - Added a user generator based on Faker library and generated 100 random user to the database
  - Implemented facebook loging strategy using passport js
  - Added app to facebook developer
  - Can redirect on login and store get data about logging in user, yey :D 
  - Implemented using the passport library a token system on server and client side for authentication
  - Cleanup on server side mostly done

### Features to be done soon
  - Pack it up and deploy to either heroku or firebase